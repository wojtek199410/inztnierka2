package com.example.demo.Services;

import com.example.demo.model.entities.Plec;
import com.example.demo.repositories.PlecRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlecService {

    private PlecRepository plecRepository;

    @Autowired
    public PlecService(PlecRepository plecRepository) {
        this.plecRepository = plecRepository;
    }

    public Optional<Plec> findById(Long id){
        return plecRepository.findById(id);
    }
    public Optional<Plec> findByNazwa(String nazwa){
        return plecRepository.findByNazwa(nazwa);
    }

    public Iterable<Plec>  findAll(){
        return plecRepository.findAll();
    }

    public Plec save (Plec plec){
        return plecRepository.save(plec);
    }
    public void deleteById(Long id){

        plecRepository.deleteById(id);
    }


}
