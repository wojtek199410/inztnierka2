package com.example.demo.Services;

import com.example.demo.model.entities.Populacja;
import com.example.demo.repositories.PopulacjaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PopulacjaService {

    private PopulacjaRepository populacjaRepository;

    @Autowired
    public PopulacjaService(PopulacjaRepository populacjaRepository) {
        this.populacjaRepository = populacjaRepository;
    }

    public Iterable<Populacja>  findAll(){
     return    populacjaRepository.findAll();
    }

    public Iterable<Populacja> findByWojeAndWiekGreaterThanEqualAndWiekLessThanEqualAndRok(String woje, int wiek1,int wiek2, int rok){
        return populacjaRepository.findByWojeAndWiekGreaterThanEqualAndWiekLessThanEqualAndRok(woje, wiek1,wiek2, rok);
    }

   public Iterable<Populacja> findByWojeAndWiekGreaterThanEqualAndWiekLessThanEqualAndRokGreaterThanEqualAndRokLessThanEqual(String woje, int wiek1,int wiek2, int rok1, int rok2){
        return populacjaRepository.findByWojeAndWiekGreaterThanEqualAndWiekLessThanEqualAndRokGreaterThanEqualAndRokLessThanEqual(woje, wiek1, wiek2, rok1, rok2);
   };

    public Iterable<Populacja> findByWiekGreaterThanEqualAndWiekLessThanEqualAndRok(int wiek1, int wiek2, int rok){
     return    populacjaRepository.findByWiekGreaterThanEqualAndWiekLessThanEqualAndRok(wiek1, wiek2, rok);
    };

   public Iterable<Populacja> findByWiekGreaterThanEqualAndWiekLessThanEqualAndRokGreaterThanEqualAndRokLessThanEqual(int wiek1, int wiek2, int rok1, int rok2){
       return populacjaRepository.findByWiekGreaterThanEqualAndWiekLessThanEqualAndRokGreaterThanEqualAndRokLessThanEqual(wiek1,wiek2, rok1, rok2);
   };

    public int sumaMzn(ArrayList<Populacja> populacje){
        int suma = 0;

        for (Populacja populacja: populacje) {
            suma = suma + populacja.getMzn();
        }
        return suma;
    }

    public int sumaKbt(ArrayList<Populacja> populacje){
        int suma = 0;

        for (Populacja populacja: populacje) {
            suma = suma + populacja.getMzn();
        }
        return suma;
    }

    public int sumaOgolnie(ArrayList<Populacja> populacje){
        int suma = 0;

        for (Populacja populacja: populacje) {
            suma = suma + populacja.getOgolnie();
        }
        return suma;
    }




}
