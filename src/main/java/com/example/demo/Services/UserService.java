package com.example.demo.Services;

import com.example.demo.model.entities.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}