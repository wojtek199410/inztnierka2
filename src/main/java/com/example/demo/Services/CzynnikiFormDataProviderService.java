package com.example.demo.Services;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class CzynnikiFormDataProviderService {



    private List<Integer> rokPocz = Arrays.asList(

           2009,
           2010,
           2011,
           2012,
           2013,
           2014,
           2015,
           2016,
           2017
    );

    private List<Integer> rokKon = Arrays.asList(

            2009,
            2010,
            2011,
            2012,
            2013,
            2014,
            2015,
            2016,
            2017
    );

   private List<Integer> wiekDolny = Arrays.asList(
        0,
        5,
        10,
        15,
        20,
        25,
        30,
        35,
        40,
        45,
        50,
        55,
        60,
        65,
        70,
        75,
        80,
        85
    );

    private List<Integer> wiekGorny = Arrays.asList(
            4,
            9,
            14,
            19,
            24,
            29,
            34,
            39,
            44,
            49,
            54,
            59,
            64,
            69,
            74,
            79,
            84,
            85
    );

    private List<String> zachorowaniaZgony = Arrays.asList(
            "zachorowania",
            "zgony",
            "ogólnie"
    );

    public List<String> getZachorowaniaZgony() {
        return zachorowaniaZgony;
    }

    public void setZachorowaniaZgony(List<String> zachorowaniaZgony) {
        this.zachorowaniaZgony = zachorowaniaZgony;
    }

    public List<Integer> getRokPocz() {
        return rokPocz;
    }

    public void setRokPocz(List<Integer> rokPocz) {
        this.rokPocz = rokPocz;
    }

    public List<Integer> getRokKon() {
        return rokKon;
    }

    public void setRokKon(List<Integer> rokKon) {
        this.rokKon = rokKon;
    }

    public List<Integer> getWiekDolny() {
        return wiekDolny;
    }

    public void setWiekDolny(List<Integer> wiekDolny) {
        this.wiekDolny = wiekDolny;
    }

    public List<Integer> getWiekGorny() {
        return wiekGorny;
    }

    public void setWiekGorny(List<Integer> wiekGorny) {
        this.wiekGorny = wiekGorny;
    }
}
