package com.example.demo.Services;

import com.example.demo.model.Dto.PacjentDto;
import com.example.demo.model.entities.*;
import com.example.demo.repositories.PacjentRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PacjentService {
    private PacjentRepository pacjentRepository;
    private RakPlucService rakPlucService;
    private CzynnikRyzService czynnikRyzService;
    private WojewodztwoService wojewodztwoService;
    private  PlecService plecService;

    public PacjentService(PacjentRepository pacjentRepository, RakPlucService rakPlucService, CzynnikRyzService czynnikRyzService, WojewodztwoService wojewodztwoService, PlecService plecService) {
        this.pacjentRepository = pacjentRepository;
        this.rakPlucService = rakPlucService;
        this.czynnikRyzService = czynnikRyzService;
        this.wojewodztwoService = wojewodztwoService;
        this.plecService = plecService;
    }

    public Optional<Pacjent> findById(Long id){
        return pacjentRepository.findById(id);
    }
    public Optional<Pacjent> findByPesel(String pesel){
        return pacjentRepository.findByPesel(pesel);
    }

    public Iterable<Pacjent>  findAll(){
        return pacjentRepository.findAll();
    }

    public Pacjent save (Pacjent pacjent){
        return pacjentRepository.save(pacjent);
    }
    public void deleteById(Long id){

        pacjentRepository.deleteById(id);
    }

   public Iterable<Pacjent> findByRakPlucId(Long id){
       return pacjentRepository.findByRakPlucId(id);
   };

//    public Iterable<Pacjent> findByCzynniki(int wiek1, int wiek2, long plec, long rak, int rok1, int rok2, long wojewodztwo ){
//        return pacjentRepository.findByWiekGreaterThanEqualAndWiekLessThanEqualAndPlecIdAndRakPlucIdAndDataZachorowaniaGreaterThanEqualAndDataZachorowaniaLessThanEqualAndWojewodztwoId(
//         wiek1,  wiek2,  plec, rak,  rok1,  rok2,  wojewodztwo
//        );
//    }

    public  Pacjent dtoiIntoPatient(PacjentDto pacjentDto){

        Pacjent pacjent = new Pacjent();
        RakPluc rakPluc = rakPlucService.findByNazwa(pacjentDto.getRakPluc()).get();
        Wojewodztwo wojewodztwo = wojewodztwoService.findByNazwa(pacjentDto.getWojewodztwo()).get();
        Plec plec = plecService.findByNazwa(pacjentDto.getPlec()).get();
        String[] czynnikiRyzyka = pacjentDto.getCzynnikRyzyka();


        ArrayList<CzynnikRyz> czynnikRyzSet = new ArrayList<CzynnikRyz>();
     //   CzynnikRyz czynnikRyz=  czynnikRyzService.findByNazwa("Historia raka w rodzinie").get();

        for (String ryzyko : czynnikiRyzyka) {
               CzynnikRyz czynnikRyz=  czynnikRyzService.findByNazwa(ryzyko).get();
               czynnikRyzSet.add(czynnikRyz);
        }
        Set<CzynnikRyz> czynnikiRyzSet2 = new HashSet<CzynnikRyz>(czynnikRyzSet);
        pacjent.setCzynnikRyzyka(czynnikiRyzSet2);
        pacjent.setRakPluc(rakPluc);
        pacjent.setWojewodztwo(wojewodztwo);
        pacjent.setPlec(plec);

        pacjent.setId(pacjentDto.getId());
        pacjent.setImie(pacjentDto.getImie());
        pacjent.setNazwisko(pacjentDto.getNazwisko());
        pacjent.setPesel(pacjentDto.getPesel());
        pacjent.setDataUrodzenia(pacjentDto.getDataUrodzenia());
        pacjent.setDataZachorowania(pacjentDto.getDataZachorowania());
        pacjent.setDataZgonu(pacjentDto.getDataZachorowania());
        pacjent.setMiasto(pacjentDto.getMiasto());
        pacjent.setUlica(pacjentDto.getUlica());
        pacjent.setNumerBudynku(pacjentDto.getNumerBudynku());
        pacjent.setNumerMieszkania(pacjentDto.getNumerMieszkania());
       return pacjent;

    }

    public PacjentDto patientIntoDto(Pacjent pacjent){
        PacjentDto pacjentDto = new PacjentDto();

        pacjentDto.setId(pacjent.getId());
        pacjentDto.setImie(pacjent.getImie());
        pacjentDto.setNazwisko(pacjent.getNazwisko());
        pacjentDto.setPesel(pacjent.getPesel());
        pacjentDto.setDataUrodzenia(pacjent.getDataUrodzenia());
        pacjentDto.setDataZachorowania(pacjent.getDataZachorowania());
        pacjentDto.setDataZgonu(pacjent.getDataZgonu());
        pacjentDto.setMiasto(pacjent.getMiasto());
        pacjentDto.setUlica(pacjent.getUlica());
        pacjentDto.setNumerBudynku(pacjent.getNumerBudynku());
        pacjentDto.setNumerMieszkania(pacjent.getNumerMieszkania());

        pacjentDto.setRakPluc(pacjent.getRakPluc().getNazwa());
        pacjentDto.setWojewodztwo(pacjent.getWojewodztwo().getNazwa());
        pacjentDto.setPlec(pacjent.getPlec().getNazwa());
       // String [] czynnikiRyzyka = new String[pacjent.getCzynnikRyzyka().size()];
        ArrayList<String> nazwyCzynnikow = new ArrayList<String>();
        for (CzynnikRyz ryzyko : pacjent.getCzynnikRyzyka()) {
            nazwyCzynnikow.add(ryzyko.getNazwa());
        }

         pacjentDto.setCzynnikRyzyka( nazwyCzynnikow.toArray(new String[0]));

        return pacjentDto;

    }
}
