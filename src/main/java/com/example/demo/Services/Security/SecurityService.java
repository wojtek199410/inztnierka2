package com.example.demo.Services.Security;

public interface SecurityService {
    boolean isAuthenticated();
    void autoLogin(String username, String password);
}
