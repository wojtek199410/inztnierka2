package com.example.demo.Services;

import com.example.demo.model.entities.CzynnikRyz;
import com.example.demo.repositories.CzynnikRyzRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CzynnikRyzService {

    private CzynnikRyzRepository czynnikRyzRepository;

    @Autowired
    public CzynnikRyzService(CzynnikRyzRepository czynnikRyzRepository) {
        this.czynnikRyzRepository = czynnikRyzRepository;
    }

    public Optional<CzynnikRyz> findById(Long id){
        return czynnikRyzRepository.findById(id);
    }
    public Optional<CzynnikRyz> findByNazwa(String nazwa){
        return czynnikRyzRepository.findByNazwa(nazwa);
    }

    public Iterable<CzynnikRyz>  findAll(){
        return czynnikRyzRepository.findAll();
    }

    public CzynnikRyz save (CzynnikRyz czynnikRyz){
        return czynnikRyzRepository.save(czynnikRyz);
    }
    public void deleteById(Long id){
        czynnikRyzRepository.deleteById(id);
    }

}
