package com.example.demo.Services.FiltrServices;

import com.example.demo.Services.*;
import com.example.demo.model.entities.CzynnikRyz;
import com.example.demo.model.entities.Pacjent;
import com.example.demo.model.entities.RakPluc;
import com.example.demo.model.entities.Wojewodztwo;
import com.example.demo.model.formularze.NowotworyProcentyFormularz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class NowotworyProcentyFiltrService {

    private PacjentService pacjentService;
    private CzynnikRyzService czynnikRyzService;
    private RakPlucService rakPlucService;
    private WojewodztwoService wojewodztwoService;
    private PlecService plecService;
    private PopulacjaService populacjaService;


    @Autowired
    public NowotworyProcentyFiltrService(PacjentService pacjentService, CzynnikRyzService czynnikRyzService, RakPlucService rakPlucService, WojewodztwoService wojewodztwoService, PlecService plecService, PopulacjaService populacjaService) {
        this.pacjentService = pacjentService;
        this.czynnikRyzService = czynnikRyzService;
        this.rakPlucService = rakPlucService;
        this.wojewodztwoService = wojewodztwoService;
        this.plecService = plecService;
        this.populacjaService = populacjaService;
    }






    //wpierw wfiltrowuje liste pacjentow o zadanych parametrach,






    //sprawdza czy pacjent spełnia zadane w suroweWspFormularz wymagania, używana w funkcji wyfiltrujPacjentow
    public boolean filtruj(Pacjent pacjent, NowotworyProcentyFormularz nowotworyProcentyFormularz){

        boolean wskaznik = true;

        if (!(nowotworyProcentyFormularz.getPlec().equals("ogółem"))){
            if (!(nowotworyProcentyFormularz.getPlec().equals(pacjent.getPlec().getNazwa()))){
                wskaznik = false;
            }
        }
        if (nowotworyProcentyFormularz.getZachorowaniaZgony().equals("zachorowania")) {
            if (!(nowotworyProcentyFormularz.getRokPocz()<=pacjent.getDataZachorowania().getYear()
                    && pacjent.getDataZachorowania().getYear()<=nowotworyProcentyFormularz.getRokKon())){
                wskaznik = false;
            }
        }
        else{
            if (!(nowotworyProcentyFormularz.getRokPocz()<=pacjent.getDataZgonu().getYear()
                    && pacjent.getDataZgonu().getYear()<=nowotworyProcentyFormularz.getRokKon())){
                wskaznik = false;
            }
        }

        if(!(nowotworyProcentyFormularz.getWiekGorny()==85)){
            if (!(nowotworyProcentyFormularz.getWiekDolny()<= pacjent.getWiek() &&  pacjent.getWiek()<=nowotworyProcentyFormularz.getWiekGorny())){
                wskaznik = false;
            }
        }
        else {
            if(!(nowotworyProcentyFormularz.getWiekDolny()<=pacjent.getWiek())){
                wskaznik = false;
            }
        }
        if(!nowotworyProcentyFormularz.getCzynnikiRyzyka()[0].equals("Ogółem")){
            for (String czynnikRyzyka: nowotworyProcentyFormularz.getCzynnikiRyzyka()) {
                if(!(nazwy(pacjent).contains(czynnikRyzyka))){
                    wskaznik = false;
                }
            }
        }
        return wskaznik;
    }

    public ArrayList<String> nazwy(Pacjent pacjent){
        ArrayList<String> nazwy = new ArrayList<>();
        for (CzynnikRyz czynnikRyz: pacjent.getCzynnikRyzyka()) {
            nazwy.add(czynnikRyz.getNazwa());
        }
        return nazwy;
    }





    public  ArrayList<Pacjent> wyfiltrujPacjentowDoUdzialuNowotworow(NowotworyProcentyFormularz nowotworyProcentyFormularz){

        ArrayList<Pacjent> pacjenciWyfiltrowani = new ArrayList<Pacjent>();
        //ArrayList<CzynnikRyz> czynnikRyzSet = (ArrayList<CzynnikRyz>) czynnikRyzService.findAll();
        ArrayList<Wojewodztwo> wojewodztwa = (ArrayList<Wojewodztwo>) wojewodztwoService.findAll();
        for(Pacjent pacjent : pacjentService.findAll()){
            if (filtruj(pacjent, nowotworyProcentyFormularz)){
                pacjenciWyfiltrowani.add(pacjent);
            }
        }

        return pacjenciWyfiltrowani;
    }

    public HashMap<String, Float> nowotworyUdzialy (NowotworyProcentyFormularz nowotworyProcentyFormularz){
        HashMap<String, Float> rakiProcenty = new HashMap<String, Float>();
        ArrayList<Pacjent> pacjenci = wyfiltrujPacjentowDoUdzialuNowotworow(nowotworyProcentyFormularz);
        ArrayList<RakPluc> rakiPluc = jakieRakie(pacjenci);
        int pula = pacjenci.size();

        for (RakPluc rakPluc: rakiPluc) {
            int licznik = 0;
            for ( Pacjent pacjent: pacjenci) {
                if (pacjent.getRakPluc().getNazwa().equals(rakPluc.getNazwa()))
                {licznik++;};
            }
            float wynik = ((float) licznik/ (float) pula)*100;
            rakiProcenty.put(rakPluc.getNazwa(), wynik);

        }

        return rakiProcenty;

    }

    public ArrayList<RakPluc> jakieRakie (ArrayList<Pacjent> pacjents){
        ArrayList<RakPluc> raki = new ArrayList<>();

        for (Pacjent pacjent: pacjents) {
            if (!raki.contains(pacjent.getRakPluc())){
                raki.add(pacjent.getRakPluc());
            }
        }

        return raki;
    }






}
