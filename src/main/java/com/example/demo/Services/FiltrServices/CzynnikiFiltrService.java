package com.example.demo.Services.FiltrServices;

import com.example.demo.Services.PacjentService;
import com.example.demo.Services.PlecService;
import com.example.demo.Services.RakPlucService;
import com.example.demo.Services.WojewodztwoService;
import com.example.demo.model.entities.CzynnikRyz;
import com.example.demo.model.formularze.CzynnikiFormularz;
import com.example.demo.model.entities.Pacjent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

@Service
public class CzynnikiFiltrService {

    private PacjentService pacjentService;
    private RakPlucService rakPlucService;
    private WojewodztwoService wojewodztwoService;
    private PlecService plecService;

    @Autowired
    public CzynnikiFiltrService(PacjentService pacjentService,RakPlucService rakPlucService,WojewodztwoService wojewodztwoService,PlecService plecService ) {
        this.pacjentService = pacjentService;
        this.plecService = plecService;
        this.rakPlucService = rakPlucService;
        this.wojewodztwoService = wojewodztwoService;
    }

    public ArrayList<Pacjent> wyfiltrujPacjentow(CzynnikiFormularz czynnikiFormularz){

        ArrayList<Pacjent> pacjenciWyfiltrowani = new ArrayList<Pacjent>();

        for(Pacjent pacjent : pacjentService.findAll()){
            if (filtruj(pacjent, czynnikiFormularz)){
                    pacjenciWyfiltrowani.add(pacjent);
            }
        }

        return pacjenciWyfiltrowani;
    }

    public ArrayList<Pacjent> wyfiltrujPacjentow2(CzynnikiFormularz czynnikiFormularz){

        ArrayList<Pacjent> pacjenciWyfiltrowani = new ArrayList<Pacjent>();

//        pacjenciWyfiltrowani = (ArrayList<Pacjent>) pacjentService.findByCzynniki(czynnikiFormularz.getWiekDolny(), czynnikiFormularz.getWiekGorny(),  plecService.findByNazwa(czynnikiFormularz.getPlec()).get().getId(),
//                rakPlucService.findByNazwa(czynnikiFormularz.getRak()).get().getId(), czynnikiFormularz.getRokPocz(), czynnikiFormularz.getRokKon(),
//                wojewodztwoService.findByNazwa(czynnikiFormularz.getWojewodztwo()).get().getId()
//                );

        return pacjenciWyfiltrowani;
    }


    public boolean filtruj(Pacjent pacjent, CzynnikiFormularz czynnikiFormularz){

        boolean wskaznik = true;

        if (!(czynnikiFormularz.getPlec().equals("ogółem"))){
            if (!(czynnikiFormularz.getPlec().equals(pacjent.getPlec().getNazwa()))){
                wskaznik = false;
            }
        }

        if (!(czynnikiFormularz.getRak().equals("Nowotwory płuc ogółem") )){
            if (!(czynnikiFormularz.getRak().equals(pacjent.getRakPluc().getNazwa()))){
                wskaznik = false;
            }
        }

        if(!(czynnikiFormularz.getWojewodztwo().equals("Cała Polska"))){
            if (!(czynnikiFormularz.getWojewodztwo().equals(pacjent.getWojewodztwo().getNazwa()))){
                wskaznik = false;
            }
        }

        if (czynnikiFormularz.getZachorowaniaZgony().equals("zachorowania")) {
            if (!(czynnikiFormularz.getRokPocz()<=pacjent.getDataZachorowania().getYear()
                    && pacjent.getDataZachorowania().getYear()<=czynnikiFormularz.getRokKon())){
                wskaznik = false;
            }
        }
        else{
            if (!(czynnikiFormularz.getRokPocz()<=pacjent.getDataZgonu().getYear()
                    && pacjent.getDataZgonu().getYear()<=czynnikiFormularz.getRokKon())){
                wskaznik = false;
            }
        }

        if(!(czynnikiFormularz.getWiekGorny()==85)){
            if (!(czynnikiFormularz.getWiekDolny()<= pacjent.getWiek() &&  pacjent.getWiek()<=czynnikiFormularz.getWiekGorny())){
                wskaznik = false;
            }
        }
        else {
            if(!(czynnikiFormularz.getWiekDolny()<=pacjent.getWiek())){
                wskaznik = false;
            }
        }


        return wskaznik;
    }

    public HashMap<String, Float> czynnikiProcenty(ArrayList<Pacjent> pacjenci){

        HashMap<String,Float> nazwyProcenty = new HashMap<>();
        ArrayList<CzynnikRyz> nazwyCzynnikow = jakieCzynniki(pacjenci);
        for (CzynnikRyz czynnikRyz : nazwyCzynnikow){
            float liczbaCzynnikow = 0;
            for (Pacjent pacjent : pacjenci){
                if (pacjent.getCzynnikRyzyka().contains(czynnikRyz)){
                    liczbaCzynnikow++;
                }
            }
            float procent = (liczbaCzynnikow/pacjenci.size())*100;
            nazwyProcenty.put(czynnikRyz.getNazwa(), procent);
        }
        return nazwyProcenty;
    }


    public ArrayList<CzynnikRyz> jakieCzynniki(ArrayList<Pacjent> pacjenci){
        ArrayList<CzynnikRyz> nazwyCzynnikow = new ArrayList<CzynnikRyz>();

        for (Pacjent pacjent : pacjenci){
            Set<CzynnikRyz> czynnikRyzs =  pacjent.getCzynnikRyzyka();
            for (CzynnikRyz czynnikRyz : czynnikRyzs){
                if (!nazwyCzynnikow.contains(czynnikRyz)){
                    nazwyCzynnikow.add(czynnikRyz);
                }
            }


        }
        if (pacjenci==null){
            return null;
        }
        else return nazwyCzynnikow;

    }






}
