package com.example.demo.Services.FiltrServices;

import com.example.demo.Services.*;
import com.example.demo.model.*;
import com.example.demo.model.entities.*;
import com.example.demo.model.formularze.SuroweWspFormularz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class SuroweWspFiltrService {

    private PacjentService pacjentService;
    private CzynnikRyzService czynnikRyzService;
    private RakPlucService rakPlucService;
    private WojewodztwoService wojewodztwoService;
    private PlecService plecService;
    private PopulacjaService populacjaService;

    @Autowired
    public SuroweWspFiltrService (CzynnikRyzService czynnikRyzService,PopulacjaService populacjaService,PacjentService pacjentService,RakPlucService rakPlucService,WojewodztwoService wojewodztwoService,PlecService plecService ) {
        this.pacjentService = pacjentService;
        this.plecService = plecService;
        this.rakPlucService = rakPlucService;
        this.wojewodztwoService = wojewodztwoService;
        this.populacjaService = populacjaService;
        this.czynnikRyzService = czynnikRyzService;

    }

    public HashMap<String, Float> wojewodztwaSuroweWsp(SuroweWspFormularz suroweWspFormularz){

        ArrayList<Wynik> wojePopulacje = wojewodztwaPopulacje(suroweWspFormularz.getWiekDolny(), suroweWspFormularz.getWiekGorny(), suroweWspFormularz.getRokPocz(), suroweWspFormularz.getRokKon(), suroweWspFormularz.getPlec());
        ArrayList<Wynik> wojPacjenci = wyfiltrujPoliczWedlugWojewodztwPacjentow(suroweWspFormularz);
        HashMap<String, Float> czynnikiSurowe = new HashMap<>();

        for (int i = 0; i < wojePopulacje.size(); i++){
            float wynik = ( (float)  wojPacjenci.get(i).getWartosc()/  wojePopulacje.get(i).getWartosc() ) *100000;
            czynnikiSurowe.put(wojePopulacje.get(i).getNazwa(), wynik);
        }



        return czynnikiSurowe;

    }




    //wpierw wfiltrowuje liste pacjentow o zadanych parametrach,
    //a następnie sprawdza i wyrzuca ile jest pacjentów w jakim wojewodztwie
    public  ArrayList<Wynik> wyfiltrujPoliczWedlugWojewodztwPacjentow(SuroweWspFormularz suroweWspFormularz){

       // HashMap<String, Integer> wojewodztwaPacjenci = new HashMap<>();
        ArrayList<Wynik> wojewodztwaPacjenci = new ArrayList<>();
        ArrayList<Pacjent> pacjenciWyfiltrowani = new ArrayList<Pacjent>();
       //ArrayList<CzynnikRyz> czynnikRyzSet = (ArrayList<CzynnikRyz>) czynnikRyzService.findAll();
        ArrayList<Wojewodztwo> wojewodztwa = (ArrayList<Wojewodztwo>) wojewodztwoService.findAll();
        for(Pacjent pacjent : pacjentService.findAll()){
            if (filtruj(pacjent, suroweWspFormularz)){
                pacjenciWyfiltrowani.add(pacjent);
            }
        }

        for (Wojewodztwo wojewodztwo: wojewodztwa) {
            int licznik = 0;
            for (Pacjent pacjent: pacjenciWyfiltrowani) {
                if (pacjent.getWojewodztwo().getNazwa().equals(wojewodztwo.getNazwa())){
                    licznik++;
                }
            }
            wojewodztwaPacjenci.add(new Wynik(wojewodztwo.getNazwa(), licznik));
        }



        return wojewodztwaPacjenci;
    }





    //sprawdza czy pacjent spełnia zadane w suroweWspFormularz wymagania, używana w funkcji wyfiltrujPacjentow
    public boolean filtruj(Pacjent pacjent, SuroweWspFormularz suroweWspFormularz){

        boolean wskaznik = true;

        if (!(suroweWspFormularz.getPlec().equals("ogółem"))){
            if (!(suroweWspFormularz.getPlec().equals(pacjent.getPlec().getNazwa()))){
                wskaznik = false;
            }
        }

        if (!(suroweWspFormularz.getRak().equals("Nowotwory płuc ogółem") )){
            if (!(suroweWspFormularz.getRak().equals(pacjent.getRakPluc().getNazwa()))){
                wskaznik = false;
            }
        }

        if (suroweWspFormularz.getZachorowaniaZgony().equals("zachorowania")) {
            if (!(suroweWspFormularz.getRokPocz()<=pacjent.getDataZachorowania().getYear()
                    && pacjent.getDataZachorowania().getYear()<=suroweWspFormularz.getRokKon())){
                wskaznik = false;
            }
        }
        else{
            if (!(suroweWspFormularz.getRokPocz()<=pacjent.getDataZgonu().getYear()
                    && pacjent.getDataZgonu().getYear()<=suroweWspFormularz.getRokKon())){
                wskaznik = false;
            }
        }

        if(!(suroweWspFormularz.getWiekGorny()==85)){
            if (!(suroweWspFormularz.getWiekDolny()<= pacjent.getWiek() &&  pacjent.getWiek()<=suroweWspFormularz.getWiekGorny())){
                wskaznik = false;
            }
        }
        else {
            if(!(suroweWspFormularz.getWiekDolny()<=pacjent.getWiek())){
                wskaznik = false;
            }
        }
        if(!suroweWspFormularz.getCzynnikiRyzyka()[0].equals("Ogółem")){
            for (String czynnikRyzyka: suroweWspFormularz.getCzynnikiRyzyka()) {
                if(!(nazwy(pacjent).contains(czynnikRyzyka))){
                    wskaznik = false;
                }
            }
        }
        return wskaznik;
    }

    public ArrayList<String> nazwy(Pacjent pacjent){
        ArrayList<String> nazwy = new ArrayList<>();
        for (CzynnikRyz czynnikRyz: pacjent.getCzynnikRyzyka()) {
            nazwy.add(czynnikRyz.getNazwa());
        }
        return nazwy;
    }




    public ArrayList<Wynik> wojewodztwaPopulacje(int wiek1, int wiek2 , int rok1, int rok2, String plec){

       // HashMap<String, Integer> wojewodztwaPopulacje = new HashMap<>();
        ArrayList<Wynik> wojewodztwaPopulacje = new ArrayList<>();
        ArrayList<Wojewodztwo> wojewodztwa = (ArrayList<Wojewodztwo>) wojewodztwoService.findAll();
        ArrayList<Populacja> populacje = (ArrayList<Populacja>) populacjaService.findByWiekGreaterThanEqualAndWiekLessThanEqualAndRokGreaterThanEqualAndRokLessThanEqual(wiek1, wiek2, rok1, rok2);
        for (Wojewodztwo wojewodztwo: wojewodztwa) {
            ArrayList<Populacja> pop = new ArrayList<>();
            for (Populacja populacja: populacje) {
                if (populacja.getWoje().equals(wojewodztwo.getNazwa())){
                    pop.add(populacja);
                }
            }
            if (plec.equals("Mężczyzna")){
                wojewodztwaPopulacje.add(new Wynik(wojewodztwo.getNazwa(), populacjaService.sumaMzn(pop)));
            }
            else if (plec.equals("Kobieta")){
                wojewodztwaPopulacje.add(new Wynik(wojewodztwo.getNazwa(), populacjaService.sumaKbt(pop)));
            }
            else {
                wojewodztwaPopulacje.add(new Wynik(wojewodztwo.getNazwa(), populacjaService.sumaOgolnie(pop)));
            }
        }


        return  wojewodztwaPopulacje;
    }

    public  ArrayList<Pacjent> wyfiltrujPacjentowDoUdzialuNowotworow(SuroweWspFormularz suroweWspFormularz){

        ArrayList<Pacjent> pacjenciWyfiltrowani = new ArrayList<Pacjent>();
        //ArrayList<CzynnikRyz> czynnikRyzSet = (ArrayList<CzynnikRyz>) czynnikRyzService.findAll();
        ArrayList<Wojewodztwo> wojewodztwa = (ArrayList<Wojewodztwo>) wojewodztwoService.findAll();
        for(Pacjent pacjent : pacjentService.findAll()){
            if (filtruj(pacjent, suroweWspFormularz)){
                pacjenciWyfiltrowani.add(pacjent);
            }
        }



        return pacjenciWyfiltrowani;
    }

    public HashMap<String, Float> nowotworyUdzialy (SuroweWspFormularz suroweWspFormularz){
        HashMap<String, Float> rakiProcenty = new HashMap<String, Float>();
        ArrayList<Pacjent> pacjenci = wyfiltrujPacjentowDoUdzialuNowotworow(suroweWspFormularz);
        ArrayList<RakPluc> rakiPluc = (ArrayList<RakPluc>) rakPlucService.findAll();
        int pula = pacjenci.size();

        for (RakPluc rakPluc: rakiPluc) {
            int licznik = 0;
            for ( Pacjent pacjent: pacjenci) {
                if (pacjent.getRakPluc().getNazwa().equals(rakPluc.getNazwa()));
                licznik++;
            }
            float wynik = (float) licznik/pula;
            rakiProcenty.put(rakPluc.getNazwa(), wynik);

        }



        return null;

    }






}
