package com.example.demo.Services;

import com.example.demo.model.entities.Wojewodztwo;
import com.example.demo.repositories.WojewodztwoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WojewodztwoService {

    private WojewodztwoRepository wojewodztwoRepository;

    @Autowired
    public WojewodztwoService(WojewodztwoRepository wojewodztwoRepository) {
        this.wojewodztwoRepository= wojewodztwoRepository;
    }

    public Optional<Wojewodztwo> findById(Long id){
        return wojewodztwoRepository.findById(id);
    }
    public Optional<Wojewodztwo> findByNazwa(String nazwa){
        return wojewodztwoRepository.findByNazwa(nazwa);
    }

    public Iterable<Wojewodztwo>  findAll(){
        return wojewodztwoRepository.findAll();
    }

    public Wojewodztwo save (Wojewodztwo wojewodztwo){
        return wojewodztwoRepository.save(wojewodztwo);
    }
    public void deleteById(Long id){

        wojewodztwoRepository.deleteById(id);
    }

}
