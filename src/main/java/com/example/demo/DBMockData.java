package com.example.demo;

import com.example.demo.Services.*;
import com.example.demo.model.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class DBMockData {

    private PacjentService pacjentService;
    private CzynnikRyzService czynnikRyzService;
    private RakPlucService rakPlucService;
    private WojewodztwoService wojewodztwoService;
    private PlecService plecService;


    @Autowired
    public DBMockData(PlecService plecService,PacjentService pacjentService, CzynnikRyzService czynnikRyzService, RakPlucService rakPlucService, WojewodztwoService wojewodztwoService) {
        this.pacjentService = pacjentService ;
        this.czynnikRyzService = czynnikRyzService;
        this.rakPlucService =rakPlucService;
        this.wojewodztwoService = wojewodztwoService;
        this.plecService = plecService;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void fill() {

//        if(pacjentRepository.findAll().iterator().hasNext()==false ) {
//
//
//            Set<CzynnikRyzyka> czynnikRyzykas = new HashSet<>();
//            czynnikRyzykas.add(CzynnikRyzyka.PALENIE);
//            czynnikRyzykas.add(CzynnikRyzyka.HISTORIA_RAKA_W_RODZINIE);
//           // Pacjent pacjent = new Pacjent("Wojciech", "Kamiński", "1234", "Radzików", "Zaje", 34, 34, "lubuskie",czynnikRyzykas, RakPluc.RAK_GRUCZOŁOWY_BRODAWKOWY);
//
//       //    pacjentRepository.save(pacjent);
//        }


        if(czynnikRyzService.findAll().iterator().hasNext()==false){
//            List<Cz> czynnikiRyzyka = Arrays.asList(
//                    CzynnikRyzyka.ZANIECZYSZCZENIE_POWIETRZA,
//                    CzynnikRyzyka.HISTORIA_RAKA_W_RODZINIE,
//                    CzynnikRyzyka.AZBEST,
//                    CzynnikRyzyka.INFEKCJE_PŁUC,
//                    CzynnikRyzyka.PALENIE,
//                    CzynnikRyzyka.RADON
//            );

            czynnikRyzService.save(new CzynnikRyz("Zanieczyszczenie powietrza"));
            czynnikRyzService.save(new CzynnikRyz("Historia raka w rodzinie"));
            czynnikRyzService.save(new CzynnikRyz("Ekspozycja na azbest"));
            czynnikRyzService.save(new CzynnikRyz("Infekcje płuc"));
            czynnikRyzService.save(new CzynnikRyz("Palenie papierosów"));
            czynnikRyzService.save(new CzynnikRyz("Ekspozycja na radon"));
        }

        if(rakPlucService.findAll().iterator().hasNext()==false){

            List<String> raczki = Arrays.asList("Rak gruczołowy tapetujący (lepidic adenocarcinoma)",
                    "Rak gruczołowy groniasty (acinar adenocarcinoma)",
                    "Rak gruczołowy brodawkowy (papillary adenocarcinoma)",
                    "Rak gruczołowy drobnobrodawkowy (micropapillary adenocarcinoma)",
                    "Rak gruczłowy lity (solid adenocarcinoma)",
                    "Rak gruczołowy naciekający z wytwarzaniem śluzu (invasive mucinous adenocarcinoma)",
                    "Rak gruczołowy naciekający mieszany z wytwarzaniem lub bez wytwarzania śluzu (invasive mixed mucinous and nonmucinous adenocarcinoma)",
                    "Rak gruczołowy koloidalny (colloid adenocarcinoma)",
                    "Rak gruczołowy z komórek typu płodowego (fetal adenocarcinoma)",
                    "Rak gruczołowy z komórek typu jelitowego (enteric adenocarcinoma)",
                    "Rak gruczołowy o niewielkiej inwazyjności z wytwarzaniem śluzu (minimally invasive mucinous adenocarcinoma)",
                    "Rak gruczołowy o niewielkiej inwazyjności bez wytwarzania śluzu (minimally invasive nonmucinous adenocarcinoma)",
                    "Atypowa hiperplazja gruczołowa (atypical adenomatous hiperplasia)",
                    "Rak gruczołowy in situ z wytwarzaniem (mucinous adenocarcinoma in situ)",
                    "Rak gruczołowy in situ z bez wytwarzania śluzu (nonmucinous adenocarcinoma in situ)",
                    "Rak płaskonabłonkowy rogowaciejący (keratinizing squamous-cell carcinoma)",
                    "Rak płaskonabłonkowy nierogowaciejący (non-keratinizing squamous-cell carcinoma)",
                    "Zmiana przedinwazyjna — rak płaskonabłonkowy in situ (squamous-cell carcinoma in situ)",
                    "Rak drobnokomórkowy (small-cell carcinoma) z odmianą w postaci raka złożonego (combined)"
                    ,"Rak wielkokomórkowy (large-cell carcinoma) z odmianą w postaci raka złożonego (combined)",
                    "Rakowiaki typowy i atypowy (typical and atypical carcinoids)",
                    "Zmiana przedinwazyjna — rozlana hiperplazja idiopatyczna płuc neuroendokrynnokomórkowa (diffuse idiopatic pulmonary neuroendocrine hyperplasia)",
                    "Rak wielkokomórkowy",
                    "Rak gruczołowo-płaskonabłonkowy",
                    "Rak mięsakowaty pleomorficzny (pleomorphic sarcomatoid carcinoma)",
                    "Rak wrzecionowatokomórkowy (spindle-cell sarcomatoid carcinoma)",
                    "Rak olbrzymiokomórkowy (giant-cell sarcomatoid carcinoma)",
                    "Mięsakorak (carcinosarcoma)",
                    "Blastoma płuc (pulmonary blastoma)",
                    "Rak śluzowo-naskórkowy (mucoepidermoid carcinoma)",
                    "Rak gruczołowo-torbielowaty (adenoid-cystic carcinoma)",
                    "Rak niesklasyfikowany"
            );

            for(String raczek : raczki){
                rakPlucService.save(new RakPluc(raczek));
            }



        }

        if(pacjentService.findAll().iterator().hasNext()==false){
            ArrayList<CzynnikRyz> czynnikRyzs = new ArrayList<CzynnikRyz>();
            czynnikRyzs.add(czynnikRyzService.findByNazwa("Zanieczyszczenie powietrza").get());
            czynnikRyzs.add(czynnikRyzService.findByNazwa("Ekspozycja na radon").get());
            Set<CzynnikRyz> czynnikRyzSet =  new HashSet<CzynnikRyz>(czynnikRyzs);
            RakPluc rakPluc = rakPlucService.findByNazwa("Rak gruczołowo-torbielowaty (adenoid-cystic carcinoma)").get();

            pacjentService.save(new Pacjent(czynnikRyzSet, rakPluc));
        }

        if(wojewodztwoService.findAll().iterator().hasNext()==false){
            List<String> wojewodztwa = Arrays.asList("Dolnośląskie",
                "Kujawsko-pomorskie",
                "Lubelskie",
                "Lubuskie",
                "Łódzkie",
                "Małopolskie",
                "Mazowieckie",
                "Opolskie",
                "Podkarpackie",
                "Podlaskie",
                "Pomorskie",
                "Śląskie",
                "Świętokrzyskie",
                "Warmińsko-mazurskie",
                "Wielkopolskie",
                "Zachodniopomorskie"
                );

            for(String wojewodztwo : wojewodztwa){
                wojewodztwoService.save(new Wojewodztwo(wojewodztwo));

            }
            ;
            Pacjent pacjent = pacjentService.findById(1L).get();
            pacjent.setWojewodztwo(wojewodztwoService.findById(2L).get());
            pacjentService.save(pacjent);

        }

        if(plecService.findAll().iterator().hasNext()==false){
            List<String> plcie = Arrays.asList("Mężczyzna",
                    "Kobieta"
            );

            for(String plec : plcie){
                plecService.save(new Plec(plec));
            }
            ;

        }


    }
}

