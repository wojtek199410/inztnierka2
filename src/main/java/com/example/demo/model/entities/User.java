package com.example.demo.model.entities;

import com.example.demo.model.entities.Role;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Lekarz")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String imie;
    private String nazwisko;
    private String pesel;
    private String pwz;

    private String password;

    @Transient
    private String passwordConfirm;

    @ManyToMany
    private Set<Role> roles;

    public User(Long id, String username, String imie, String nazwisko, String pesel, String pwz, String password, String passwordConfirm, Set<Role> roles) {
        this.id = id;
        this.username = username;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.pwz = pwz;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.roles = roles;
    }

    public User() {

    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getPwz() {
        return pwz;
    }

    public void setPwz(String pwz) {
        this.pwz = pwz;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}