package com.example.demo.model.entities;

import com.example.demo.model.entities.Pacjent;

import javax.persistence.*;
import java.util.Set;

@Entity
public class RakPluc {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nazwa;

    @OneToMany
    private Set<Pacjent> pacjent;

    public RakPluc(Long id, String nazwa) {
        this.id = id;
        this.nazwa = nazwa;
    }

    public RakPluc( String nazwa) {
        this.nazwa = nazwa;
    }

    public RakPluc() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Override
    public String toString() {
        return "RakPluc{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
                ", pacjent=" + pacjent +
                '}';
    }
}
