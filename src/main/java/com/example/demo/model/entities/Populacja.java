package com.example.demo.model.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Populacja {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int wiek;

    private int ogolnie;
    private int mzn;
    private int kbt;
    private String woje;
    private int rok;

    public Populacja(Long id, int wiek, int ogolnie, int mzn, int kbt, String woje, int rok) {
        this.id = id;
        this.wiek = wiek;
        this.ogolnie = ogolnie;
        this.mzn = mzn;
        this.kbt = kbt;
        this.woje = woje;
        this.rok = rok;
    }

    public Populacja() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getOgolnie() {
        return ogolnie;
    }

    public void setOgolnie(int ogolnie) {
        this.ogolnie = ogolnie;
    }

    public int getMzn() {
        return mzn;
    }

    public void setMzn(int mzn) {
        this.mzn = mzn;
    }

    public int getKbt() {
        return kbt;
    }

    public void setKbt(int kbt) {
        this.kbt = kbt;
    }

    public String getWoje() {
        return woje;
    }

    public void setWoje(String woje) {
        this.woje = woje;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    @Override
    public String toString() {
        return "Populacja{" +
                "id=" + id +
                ", wiek=" + wiek +
                ", ogolnie=" + ogolnie +
                ", mzn=" + mzn +
                ", kbt=" + kbt +
                ", woje='" + woje + '\'' +
                ", rok=" + rok +
                '}';
    }
}
