package com.example.demo.model.entities;


import com.example.demo.model.entities.Pacjent;

import javax.persistence.*;
import java.util.Set;

@Entity
public class CzynnikRyz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nazwa;


    @ManyToMany
    private Set<Pacjent> pacjent;

    public CzynnikRyz(Long id, String nazwa, Set<Pacjent> pacjent) {
        this.id = id;
        this.nazwa = nazwa;
        this.pacjent = pacjent;
    }


    public Set<Pacjent> getPacjent() {
        return pacjent;
    }

    public void setPacjent(Set<Pacjent> pacjent) {
        this.pacjent = pacjent;
    }

    public CzynnikRyz(Long id, String nazwa) {
        this.id = id;
        this.nazwa = nazwa;
    }

    public CzynnikRyz( String nazwa) {
        this.nazwa = nazwa;
    }


    public CzynnikRyz() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
