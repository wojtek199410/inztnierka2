package com.example.demo.model.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.Set;

@Entity
public class Pacjent {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String imie;
    private String nazwisko;
    private String pesel;
    private String miasto;
    private String ulica;
    private int numerBudynku;
    private int numerMieszkania;

    @ManyToOne
    private Plec plec;

    @ManyToOne
    private Wojewodztwo wojewodztwo;

    @ManyToMany
    private Set<CzynnikRyz> czynnikRyzyka;

    @ManyToOne
    private RakPluc rakPluc;

//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dataZachorowania;

//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dataUrodzenia;

//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dataZgonu;

//    @ElementCollection(targetClass = CzynnikRyzyka.class)
//    @CollectionTable
//    @Enumerated(EnumType.STRING)


    public Pacjent(Long id, String imie, String nazwisko, String pesel, String miasto, String ulica, int numerBudynku, int numerMieszkania, LocalDate dataZachorowania, LocalDate dataUrodzenia, LocalDate dataZgonu, Set<CzynnikRyz> czynnikRyzyka, RakPluc rakPluc, Plec plec, Wojewodztwo wojewodztwo) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numerBudynku = numerBudynku;
        this.numerMieszkania = numerMieszkania;
        this.dataZachorowania = dataZachorowania;
        this.dataUrodzenia = dataUrodzenia;
        this.dataZgonu = dataZgonu;
        this.czynnikRyzyka = czynnikRyzyka;
        this.rakPluc = rakPluc;
        this.plec = plec;
        this.wojewodztwo = wojewodztwo;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }



    public Pacjent(Long id, String imie, String nazwisko, String pesel, String miasto, String ulica, int numerBudynku, int numerMieszkania, Wojewodztwo wojewodztwo, LocalDate dataZachorowania, Set<CzynnikRyz> czynnikRyzyka, RakPluc rakPluc) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numerBudynku = numerBudynku;
        this.numerMieszkania = numerMieszkania;
        this.wojewodztwo = wojewodztwo;
        this.dataZachorowania = dataZachorowania;
        this.czynnikRyzyka = czynnikRyzyka;
        this.rakPluc = rakPluc;
    }

    public Pacjent(Set<CzynnikRyz> czynnikRyzyka, RakPluc rakPluc, Wojewodztwo wojewodztwo) {
        this.czynnikRyzyka = czynnikRyzyka;
        this.rakPluc = rakPluc;
        this.wojewodztwo = wojewodztwo;
    }

    public Pacjent(Set<CzynnikRyz> czynnikRyzyka, RakPluc rakPluc) {
        this.czynnikRyzyka = czynnikRyzyka;
        this.rakPluc = rakPluc;

    }

    public Pacjent(String imie, String nazwisko, String pesel, String miasto, String ulica, int numerBudynku, int numerMieszkania, Wojewodztwo wojewodztwo, RakPluc rakPluc) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numerBudynku = numerBudynku;
        this.numerMieszkania = numerMieszkania;
        this.wojewodztwo = wojewodztwo;

        this.rakPluc = rakPluc;
    }

    public Pacjent(String imie, String nazwisko, String pesel, String miasto, String ulica, int numerBudynku, int numerMieszkania, Wojewodztwo wojewodztwo) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numerBudynku = numerBudynku;
        this.numerMieszkania = numerMieszkania;
        this.wojewodztwo = wojewodztwo;
//        this.czynnikRyzyka = czynnikRyzyka;
//        this.rakPluc = rakPluc;
    }

    public Pacjent() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public int getNumerBudynku() {
        return numerBudynku;
    }

    public void setNumerBudynku(int numerBudynku) {
        this.numerBudynku = numerBudynku;
    }

    public int getNumerMieszkania() {
        return numerMieszkania;
    }

    public void setNumerMieszkania(int numerMieszkania) {
        this.numerMieszkania = numerMieszkania;
    }

    public Wojewodztwo getWojewodztwo() {
        return wojewodztwo;
    }

    public void setWojewodztwo(Wojewodztwo wojewodztwo) {
        this.wojewodztwo = wojewodztwo;
    }

    public Set<CzynnikRyz> getCzynnikRyzyka() {
        return czynnikRyzyka;
    }

    public void setCzynnikRyzyka(Set<CzynnikRyz> czynnikRyzyka) {
        this.czynnikRyzyka = czynnikRyzyka;
    }

    public RakPluc getRakPluc() {
        return rakPluc;
    }

    public void setRakPluc(RakPluc rakPluc) {
        this.rakPluc = rakPluc;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    public LocalDate getDataZachorowania() {
        return dataZachorowania;
    }

    public void setDataZachorowania(LocalDate dataZachorowania) {
        this.dataZachorowania = dataZachorowania;
    }


    public LocalDate getDataZgonu() {
        return dataZgonu;
    }

    public void setDataZgonu(LocalDate dataZgonu) {
        this.dataZgonu = dataZgonu;
    }

    public String getAdres(){
        return  getWojewodztwo().getNazwa() +" "+ getMiasto()+",ulica "+ getUlica()+ " "+getNumerBudynku()+"/"+getNumerMieszkania();
    }
    public int getWiek(){

        Period diff = Period.between(getDataUrodzenia(), LocalDate.now());

        return diff.getYears();
    }

    @Override
    public String toString() {
        return "Pacjent{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", pesel='" + pesel + '\'' +
                ", miasto='" + miasto + '\'' +
                ", ulica='" + ulica + '\'' +
                ", numerBudynku=" + numerBudynku +
                ", numerMieszkania=" + numerMieszkania +
                ", dataZachorowania=" + dataZachorowania +
                ", dataUrodzenia=" + dataUrodzenia +
                ", dataZgonu=" + dataZgonu +
                ", czynnikRyzyka=" + czynnikRyzyka +
                ", rakPluc=" + rakPluc +
                ", plec=" + plec +
                ", wojewodztwo=" + wojewodztwo +
                '}';
    }
}
