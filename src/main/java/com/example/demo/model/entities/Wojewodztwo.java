package com.example.demo.model.entities;

import com.example.demo.model.entities.Pacjent;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Wojewodztwo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nazwa;

    @OneToMany
    private Set<Pacjent> pacjent;

    public Wojewodztwo(Long id, String nazwa, Set<Pacjent> pacjent) {
        this.id = id;
        this.nazwa = nazwa;
        this.pacjent = pacjent;
    }

    public Wojewodztwo( String nazwa, Set<Pacjent> pacjent) {
        this.nazwa = nazwa;
        this.pacjent = pacjent;
    }



    public Wojewodztwo(Long id, String nazwa) {
        this.id = id;
        this.nazwa = nazwa;
    }

    public Wojewodztwo( String nazwa) {
        this.nazwa = nazwa;
    }

    public Wojewodztwo( ){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
