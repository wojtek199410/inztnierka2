package com.example.demo.model.formularze;

public class CzynnikiFormularz {

    private int rokPocz;
    private int rokKon;

    private int wiekDolny;
    private int wiekGorny;

    private String plec;
    private String wojewodztwo;

    private String zachorowaniaZgony;
    private String rak;

    public CzynnikiFormularz(int rokPocz, int rokKon, int wiekDolny, int wiekGorny, String plec, String wojewodztwo, String zachorowaniaZgony, String rak) {
        this.rokPocz = rokPocz;
        this.rokKon = rokKon;
        this.wiekDolny = wiekDolny;
        this.wiekGorny = wiekGorny;
        this.plec = plec;
        this.wojewodztwo = wojewodztwo;
        this.zachorowaniaZgony = zachorowaniaZgony;
        this.rak = rak;
    }

    public CzynnikiFormularz() {

    }

    public int getRokPocz() {
        return rokPocz;
    }

    public void setRokPocz(int rokPocz) {
        this.rokPocz = rokPocz;
    }

    public int getRokKon() {
        return rokKon;
    }

    public void setRokKon(int rokKon) {
        this.rokKon = rokKon;
    }

    public int getWiekDolny() {
        return wiekDolny;
    }

    public void setWiekDolny(int wiekDolny) {
        this.wiekDolny = wiekDolny;
    }

    public int getWiekGorny() {
        return wiekGorny;
    }

    public void setWiekGorny(int wiekGorny) {
        this.wiekGorny = wiekGorny;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    public String getWojewodztwo() {
        return wojewodztwo;
    }

    public void setWojewodztwo(String wojewodztwo) {
        this.wojewodztwo = wojewodztwo;
    }

    public String getZachorowaniaZgony() {
        return zachorowaniaZgony;
    }

    public void setZachorowaniaZgony(String zachorowaniaZgony) {
        this.zachorowaniaZgony = zachorowaniaZgony;
    }

    public String getRak() {
        return rak;
    }

    public void setRak(String rak) {
        this.rak = rak;
    }
}
