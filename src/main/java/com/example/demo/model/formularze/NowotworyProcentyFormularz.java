package com.example.demo.model.formularze;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NowotworyProcentyFormularz{

    private int rokPocz;
    private int rokKon;

    private int wiekDolny;
    private int wiekGorny;

    private String plec;


    private String[] czynnikiRyzyka;

    private String zachorowaniaZgony;


    public NowotworyProcentyFormularz(int rokPocz, int rokKon, int wiekDolny, int wiekGorny, String plec, String[] czynnikiRyzyka, String zachorowaniaZgony) {
        this.rokPocz = rokPocz;
        this.rokKon = rokKon;
        this.wiekDolny = wiekDolny;
        this.wiekGorny = wiekGorny;
        this.plec = plec;
        this.czynnikiRyzyka = czynnikiRyzyka;
        this.zachorowaniaZgony = zachorowaniaZgony;
    }

    public NowotworyProcentyFormularz() {

    }


    public int getRokPocz() {
        return rokPocz;
    }

    public void setRokPocz(int rokPocz) {
        this.rokPocz = rokPocz;
    }

    public int getRokKon() {
        return rokKon;
    }

    public void setRokKon(int rokKon) {
        this.rokKon = rokKon;
    }

    public int getWiekDolny() {
        return wiekDolny;
    }

    public void setWiekDolny(int wiekDolny) {
        this.wiekDolny = wiekDolny;
    }

    public int getWiekGorny() {
        return wiekGorny;
    }

    public void setWiekGorny(int wiekGorny) {
        this.wiekGorny = wiekGorny;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }



    public String getZachorowaniaZgony() {
        return zachorowaniaZgony;
    }

    public void setZachorowaniaZgony(String zachorowaniaZgony) {
        this.zachorowaniaZgony = zachorowaniaZgony;
    }


    public String[] getCzynnikiRyzyka() {
        return czynnikiRyzyka;
    }

    public void setCzynnikiRyzyka(String[] czynnikiRyzyka) {
        this.czynnikiRyzyka = czynnikiRyzyka;
    }
}


