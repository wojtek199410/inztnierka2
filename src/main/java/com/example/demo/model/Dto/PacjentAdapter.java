package com.example.demo.model.Dto;

public class PacjentAdapter {
    private String pesel;

    public PacjentAdapter(String pesel) {
        this.pesel = pesel;
    }
    public PacjentAdapter() {
    }


    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}
