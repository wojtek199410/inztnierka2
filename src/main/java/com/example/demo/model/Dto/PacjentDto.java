package com.example.demo.model.Dto;



import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class PacjentDto {



    private Long id;
    private String imie;
    private String nazwisko;
    private String pesel;
    private String miasto;
    private String ulica;
    private int numerBudynku;
    private int numerMieszkania;
    private String wojewodztwo;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dataZachorowania;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dataUrodzenia;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dataZgonu;

    //    @ElementCollection(targetClass = CzynnikRyzyka.class)
//    @CollectionTable
//    @Enumerated(EnumType.STRING)

    private  String[] czynnikRyzyka;

    private String rakPluc;

    private String plec;



    public PacjentDto(Long id, String imie, String nazwisko, String pesel, String miasto, String ulica, int numerBudynku, int numerMieszkania, String wojewodztwo, LocalDate dataZachorowania, LocalDate dataUrodzenia, LocalDate dataZgonu, String[] czynnikRyzyka, String rakPluc, String plec) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numerBudynku = numerBudynku;
        this.numerMieszkania = numerMieszkania;
        this.wojewodztwo = wojewodztwo;
        this.dataZachorowania = dataZachorowania;
        this.dataUrodzenia = dataUrodzenia;
        this.dataZgonu = dataZgonu;
        this.czynnikRyzyka = czynnikRyzyka;
        this.rakPluc = rakPluc;
        this.plec = plec;
    }

    public PacjentDto(Long id, String imie, String nazwisko, String pesel, String miasto, String ulica, int numerBudynku, int numerMieszkania, String wojewodztwo, LocalDate dataZachorowania, LocalDate dataUrodzenia, LocalDate dataZgonu, String[] czynnikRyzyka, String rakPluc) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.miasto = miasto;
        this.ulica = ulica;
        this.numerBudynku = numerBudynku;
        this.numerMieszkania = numerMieszkania;
        this.wojewodztwo = wojewodztwo;
        this.dataZachorowania = dataZachorowania;
        this.dataUrodzenia = dataUrodzenia;
        this.dataZgonu = dataZgonu;
        this.czynnikRyzyka = czynnikRyzyka;
        this.rakPluc = rakPluc;
    }

    public PacjentDto() {
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public int getNumerBudynku() {
        return numerBudynku;
    }

    public void setNumerBudynku(int numerBudynku) {
        this.numerBudynku = numerBudynku;
    }

    public int getNumerMieszkania() {
        return numerMieszkania;
    }

    public void setNumerMieszkania(int numerMieszkania) {
        this.numerMieszkania = numerMieszkania;
    }

    public String getWojewodztwo() {
        return wojewodztwo;
    }

    public void setWojewodztwo(String wojewodztwo) {
        this.wojewodztwo = wojewodztwo;
    }

    public LocalDate getDataZachorowania() {
        return dataZachorowania;
    }

    public void setDataZachorowania(LocalDate dataZachorowania) {
        this.dataZachorowania = dataZachorowania;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    public LocalDate getDataZgonu() {
        return dataZgonu;
    }

    public void setDataZgonu(LocalDate dataZgonu) {
        this.dataZgonu = dataZgonu;
    }

    public String[] getCzynnikRyzyka() {
        return czynnikRyzyka;
    }

    public void setCzynnikRyzyka(String[] czynnikRyzyka) {
        this.czynnikRyzyka = czynnikRyzyka;
    }

    public String getRakPluc() {
        return rakPluc;
    }

    public void setRakPluc(String rakPluc) {
        this.rakPluc = rakPluc;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    public String getAdres(){
        return this.wojewodztwo+", " + this.miasto + ", " + this.ulica + ", " + this.numerBudynku +", " +this.numerMieszkania;
    }
}

