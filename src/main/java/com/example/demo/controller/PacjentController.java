package com.example.demo.controller;

import com.example.demo.Services.*;
import com.example.demo.Services.FiltrServices.CzynnikiFiltrService;
import com.example.demo.Services.FiltrServices.NowotworyProcentyFiltrService;
import com.example.demo.Services.FiltrServices.SuroweWspFiltrService;
import com.example.demo.model.Dto.PacjentAdapter;
import com.example.demo.model.Dto.PacjentDto;
import com.example.demo.model.entities.*;
import com.example.demo.model.formularze.CzynnikiFormularz;
import com.example.demo.model.formularze.NowotworyProcentyFormularz;
import com.example.demo.model.formularze.SuroweWspFormularz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class PacjentController {

    private PacjentService pacjentService;
    private CzynnikRyzService czynnikRyzService;
    private RakPlucService rakPlucService;
    private WojewodztwoService wojewodztwoService;
    private PlecService plecService;
    private CzynnikiFormDataProviderService czynnikiFormDataProviderService;
    private CzynnikiFiltrService czynnikiFiltrService;
    private SuroweWspFiltrService suroweWspFiltrService;
    private NowotworyProcentyFiltrService nowotworyProcentyFiltrService;

    @Autowired
    public PacjentController(NowotworyProcentyFiltrService nowotworyProcentyFiltrService, SuroweWspFiltrService suroweWspFiltrService, CzynnikiFiltrService czynnikiFiltrService , CzynnikiFormDataProviderService czynnikiFormDataProviderService, PlecService plecService, PacjentService pacjentService, CzynnikRyzService czynnikRyzService, RakPlucService rakPlucService, WojewodztwoService wojewodztwoService) {
        this.pacjentService = pacjentService;
        this.czynnikRyzService = czynnikRyzService;
        this.rakPlucService = rakPlucService;
        this.wojewodztwoService = wojewodztwoService;
        this.plecService = plecService;
        this.czynnikiFormDataProviderService = czynnikiFormDataProviderService;
        this.czynnikiFiltrService = czynnikiFiltrService;
        this.suroweWspFiltrService = suroweWspFiltrService;
        this.nowotworyProcentyFiltrService  = nowotworyProcentyFiltrService;
    }

    @GetMapping("/strona_startowa")
    public String stronaStartowa(){

        return "stronaStartowa";
    }


    @GetMapping("/zarejestruj_pacjenta")
    public String pokazFormularz(Model model){
//        Pacjent pacjent = new Pacjent();
        PacjentDto pacjentDto = new PacjentDto();
        boolean wskaznikPesel = true;
        List<CzynnikRyz> czynnikiRyzyka =  (List<CzynnikRyz>) czynnikRyzService.findAll();
        List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
        List<Plec> plcie =  (List<Plec>) plecService.findAll();
        List<Wojewodztwo> wojewodztwa = (List<Wojewodztwo>) wojewodztwoService.findAll();

        model.addAttribute("pacjentDto", pacjentDto);
        model.addAttribute("raczki", raczki);
        model.addAttribute("czynnikiRyzyka", czynnikiRyzyka);
        model.addAttribute("wojewodztwa", wojewodztwa);
        model.addAttribute("plcie", plcie);
        model.addAttribute("wskaznikPesel", wskaznikPesel);
        return "formularz_rejestracji_pacjenta";
    }

    @PostMapping("/zarejestruj_pacjenta")
    public String zapiszPacjenta(@ModelAttribute("pacjentDto") PacjentDto pacjentDto, Model model){
        System.out.println(pacjentDto);
        boolean wskaznikPesel = false;
        if(pacjentService.findByPesel(pacjentDto.getPesel()).isEmpty()){
            wskaznikPesel = true;
        }

        if(wskaznikPesel){
            pacjentService.save(pacjentService.dtoiIntoPatient(pacjentDto));
            return "stronaStartowa";
        }
        else{

            List<CzynnikRyz> czynnikiRyzyka =  (List<CzynnikRyz>) czynnikRyzService.findAll();
            List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
            List<Wojewodztwo> wojewodztwa = (List<Wojewodztwo>) wojewodztwoService.findAll();
            List<Plec> plcie =  (List<Plec>) plecService.findAll();

            model.addAttribute("pacjentDto", pacjentDto);
            model.addAttribute("raczki", raczki);
            model.addAttribute("czynnikiRyzyka", czynnikiRyzyka);
            model.addAttribute("wojewodztwa", wojewodztwa);
            model.addAttribute("plcie", plcie);
            model.addAttribute("wskaznikPesel", wskaznikPesel);
            model.addAttribute("wskaznikPesel", wskaznikPesel);
            return "formularz_rejestracji_pacjenta";
        }

    }

    @GetMapping("/wyszukaj_pacjenta")
    public String wyszukajPacjenta(Model model){
       // Pacjent pacjent = new Pacjent();
        boolean wskaznikPesel = true;
        PacjentDto pacjent = new PacjentDto();
        PacjentAdapter pacjentAdapter= new PacjentAdapter();
        model.addAttribute("pacjentAdapter", pacjentAdapter);
        model.addAttribute("pacjent", pacjent);
        model.addAttribute("wskaznikPesel", wskaznikPesel);
        return "formularz_wyszukaj_pacjenta";
    }

    @PostMapping("/wyszukaj_pacjenta")
    public String wyświetlPacjenta(@ModelAttribute("pacjentAdapter") PacjentAdapter pacjentAdapter, Model model, BindingResult bindingResult){
        boolean wskaznikPesel = true;

        if(pacjentService.findByPesel(pacjentAdapter.getPesel()).isEmpty()){
            wskaznikPesel = false;
            PacjentDto pacjent = new PacjentDto();
            model.addAttribute("wskaznikPesel", wskaznikPesel);
            model.addAttribute("pacjent", pacjent);
            return "formularz_wyszukaj_pacjenta";
        }
        else{
            Pacjent pacjent =  pacjentService.findByPesel(pacjentAdapter.getPesel()).get();
            PacjentDto pacjentDto = pacjentService.patientIntoDto(pacjent);
            model.addAttribute("pacjent", pacjentDto);
            model.addAttribute("wskaznikPesel", wskaznikPesel);
            return "formularz_wyszukaj_pacjenta";
        }
    }

    @GetMapping("/wyszukaj_pacjenta_do_mod")
    public String wyszukajPacjentaDoMod(Model model){
        boolean wskaznikPesel = true;
        PacjentAdapter pacjentAdapter= new PacjentAdapter();
        model.addAttribute("pacjentAdapter", pacjentAdapter);
        model.addAttribute("wskaznikPesel", wskaznikPesel);
        return "formularz_wyszukaj_pesel_do_mod";
    }

    @PostMapping("/wyszukaj_pacjenta_do_mod")
    public String wprowadzDaneDoMod(Model model, @ModelAttribute("pacjentAdapter") PacjentAdapter pacjentAdapter){
        boolean peselWskaznik = true;
        if(pacjentService.findByPesel(pacjentAdapter.getPesel()).isEmpty()){
            peselWskaznik = false;
            model.addAttribute("pacjentAdapter", pacjentAdapter);
            model.addAttribute("peselWskaznik", peselWskaznik);
            return "formularz_wyszukaj_pesel_do_mod";
        }
        else{
            Pacjent pacjent =  pacjentService.findByPesel(pacjentAdapter.getPesel()).get();
            PacjentDto pacjentDto = pacjentService.patientIntoDto(pacjent);
//        System.out.println(pacjent.toString());

            List<CzynnikRyz> czynnikiRyzyka =  (List<CzynnikRyz>) czynnikRyzService.findAll();
            List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
            List<Wojewodztwo> wojewodztwa = (List<Wojewodztwo>) wojewodztwoService.findAll();
            List<Plec> plcie =  (List<Plec>) plecService.findAll();


            model.addAttribute("wojewodztwa", wojewodztwa);
            model.addAttribute("plcie", plcie);
            model.addAttribute("raczki", raczki);
            model.addAttribute("czynnikiRyzyka", czynnikiRyzyka);
            model.addAttribute("pacjent", pacjentDto);
            model.addAttribute("peselWskaznik", peselWskaznik);
            return "formularz_modyfikacja_danych";
        }

    }

    @PostMapping("/modyfikuj_dane_pacjenta")
    public String modyfikujPacjenta(@ModelAttribute("pacjent") PacjentDto pacjentDto, Model model){
        if (pacjentService.findByPesel(pacjentDto.getPesel()).isEmpty() ||
                pacjentService.findByPesel(pacjentDto.getPesel()).get().equals(
                pacjentService.findById(pacjentDto.getId()).get())
        ){
            Pacjent pacjent = pacjentService.dtoiIntoPatient(pacjentDto);
            pacjentService.save(pacjent);
            System.out.println(pacjent.toString());
            return "stronaStartowa";
        }
        else {
            boolean peselWskaznik = false;
       //     Pacjent pacjent =  pacjentService.findByPesel(pacjentAdapter.getPesel()).get();
             pacjentDto = pacjentService.patientIntoDto(pacjentService.findById(pacjentDto.getId()).get());
//        System.out.println(pacjent.toString());

            List<CzynnikRyz> czynnikiRyzyka =  (List<CzynnikRyz>) czynnikRyzService.findAll();
            List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
            List<Wojewodztwo> wojewodztwa = (List<Wojewodztwo>) wojewodztwoService.findAll();
            List<Plec> plcie =  (List<Plec>) plecService.findAll();


            model.addAttribute("wojewodztwa", wojewodztwa);
            model.addAttribute("plcie", plcie);
            model.addAttribute("raczki", raczki);
            model.addAttribute("czynnikiRyzyka", czynnikiRyzyka);
            model.addAttribute("pacjent", pacjentDto);
            model.addAttribute("peselWskaznik", peselWskaznik);
            return "formularz_modyfikacja_danych";
        }

    }

    @GetMapping("/formularz_filtrowanie_czynniki")
    public String filtrowanieDoCzynnikow(Model model){

        CzynnikiFormularz czynnikiFormularz = new CzynnikiFormularz();
        List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
        raczki.add(new RakPluc(33L, "Nowotwory płuc ogółem"));
        List<Wojewodztwo> wojewodztwa = (List<Wojewodztwo>) wojewodztwoService.findAll();
        wojewodztwa.add(new Wojewodztwo(17L, "Cała Polska"));
        List<Plec> plcie =  (List<Plec>) plecService.findAll();
        plcie.add(new Plec(3L, "ogółem"));
        List<Integer> rokPoczs = czynnikiFormDataProviderService.getRokPocz();
        List<Integer> rokKons = czynnikiFormDataProviderService.getRokKon();
        List<Integer> wiekDolnys = czynnikiFormDataProviderService.getWiekDolny();
        List<Integer> wiekGornys = czynnikiFormDataProviderService.getWiekGorny();
        List<String> zachorowaniaZgony = czynnikiFormDataProviderService.getZachorowaniaZgony();
        boolean wskaznikLat = true;
        boolean wskaznikWieku = true;

        model.addAttribute("czynnikFormularz", czynnikiFormularz);
        model.addAttribute("plcie", plcie);
        model.addAttribute("raczki", raczki);
        model.addAttribute("wojewodztwa", wojewodztwa);

        model.addAttribute("rokPoczs", rokPoczs);
        model.addAttribute("rokKons", rokKons);
        model.addAttribute("wiekDolnys", wiekDolnys);
        model.addAttribute("wiekGornys", wiekGornys);
        model.addAttribute("zachorowaniaZgonys", zachorowaniaZgony);

        model.addAttribute("wskaznikLat", wskaznikLat);
        model.addAttribute("wskaznikWieku", wskaznikWieku);

        return "formularz_filtrowania_wspolczynniki";
    }

    @PostMapping("/formularz_filtrowanie_czynniki")
    public String filtrowaneDoCzynnikow(@ModelAttribute("czynnikFormularz") CzynnikiFormularz czynnikiFormularz,Model model
    ){

        boolean wskaznikLat = true;
        boolean wskaznikWieku = true;

        if ((czynnikiFormularz.getRokKon()<czynnikiFormularz.getRokPocz()) || (czynnikiFormularz.getWiekGorny()<czynnikiFormularz.getWiekDolny())){

            List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
            raczki.add(new RakPluc(33L, "Nowotwory płuc ogółem"));
            List<Wojewodztwo> wojewodztwa = (List<Wojewodztwo>) wojewodztwoService.findAll();
            wojewodztwa.add(new Wojewodztwo(17L, "Cała Polska"));
            List<Plec> plcie =  (List<Plec>) plecService.findAll();
            plcie.add(new Plec(3L, "ogółem"));
            List<Integer> rokPoczs = czynnikiFormDataProviderService.getRokPocz();
            List<Integer> rokKons = czynnikiFormDataProviderService.getRokKon();
            List<Integer> wiekDolnys = czynnikiFormDataProviderService.getWiekDolny();
            List<Integer> wiekGornys = czynnikiFormDataProviderService.getWiekGorny();
            List<String> zachorowaniaZgony = czynnikiFormDataProviderService.getZachorowaniaZgony();



            model.addAttribute("czynnikFormularz", czynnikiFormularz);
            model.addAttribute("plcie", plcie);
            model.addAttribute("raczki", raczki);
            model.addAttribute("wojewodztwa", wojewodztwa);

            model.addAttribute("rokPoczs", rokPoczs);
            model.addAttribute("rokKons", rokKons);
            model.addAttribute("wiekDolnys", wiekDolnys);
            model.addAttribute("wiekGornys", wiekGornys);
            model.addAttribute("zachorowaniaZgonys", zachorowaniaZgony);

            if (czynnikiFormularz.getWiekDolny()>czynnikiFormularz.getWiekGorny()){
                wskaznikWieku = false;
            }
            if(czynnikiFormularz.getRokPocz()>czynnikiFormularz.getRokKon()){
                wskaznikLat = false;

            }
            model.addAttribute("wskaznikWieku", wskaznikWieku);
            model.addAttribute("wskaznikLat", wskaznikLat);
            return "formularz_filtrowania_wspolczynniki";
        }
        else{
            ArrayList<Pacjent> pacjenciWyfiltrowani = czynnikiFiltrService.wyfiltrujPacjentow(czynnikiFormularz);
            //   ArrayList<Pacjent> pacjenciWyfiltrowani2 = czynnikiFiltrService.wyfiltrujPacjentow2(czynnikiFormularz);
            // model.addAttribute("liczba2", pacjenciWyfiltrowani2.size());
            Map<String, Float> test = new LinkedHashMap<>();
            test = czynnikiFiltrService.czynnikiProcenty(pacjenciWyfiltrowani);
            if (test.isEmpty()){
                test.put("Brak danych z danego zakresu", 0f);
            }
            model.addAttribute("test", test);
            return "liczbaTest";
        }

    }

    @GetMapping("/formularz_filtrowanie_surowe")
    public String filtrowanieDoSurowych(Model model){

        boolean wskaznikLat = true;
        boolean wskaznikWieku = true;
        //wywołanie obiektu formularza
       SuroweWspFormularz suroweWspFormularz = new SuroweWspFormularz();


       // Wywołanie obiektów rak płuc to listy
        List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
        raczki.add(new RakPluc(33L, "Nowotwory płuc ogółem"));


        //wywołanie obiektów plec do listy
        List<Plec> plcie =  (List<Plec>) plecService.findAll();
        plcie.add(new Plec(3L, "ogółem"));

        //wywołanie obiektów czynnikRyz do listy
        List<CzynnikRyz> czynnikRyzyka = (List<CzynnikRyz>) czynnikRyzService.findAll();
        czynnikRyzyka.add(new CzynnikRyz(7L, "Ogółem"));



        List<Integer> rokPoczs = czynnikiFormDataProviderService.getRokPocz();
        List<Integer> rokKons = czynnikiFormDataProviderService.getRokKon();
        List<Integer> wiekDolnys = czynnikiFormDataProviderService.getWiekDolny();
        List<Integer> wiekGornys = czynnikiFormDataProviderService.getWiekGorny();
        List<String> zachorowaniaZgony = czynnikiFormDataProviderService.getZachorowaniaZgony();

        model.addAttribute("suroweFormularz", suroweWspFormularz);
        model.addAttribute("plcie", plcie);
        model.addAttribute("raczki", raczki);
        model.addAttribute("czynnikiRyz", czynnikRyzyka);


        model.addAttribute("rokPoczs", rokPoczs);
        model.addAttribute("rokKons", rokKons);
        model.addAttribute("wiekDolnys", wiekDolnys);
        model.addAttribute("wiekGornys", wiekGornys);
        model.addAttribute("zachorowaniaZgonys", zachorowaniaZgony);

        model.addAttribute("wskaznikLat", wskaznikLat);
        model.addAttribute("wskaznikWieku", wskaznikWieku);

        return "formularz_filtrowania_surowe";
    }


    @PostMapping("/formularz_filtrowanie_surowe123")
    public String suroweWspFiltruj(Model model, @ModelAttribute("suroweFormularz") SuroweWspFormularz suroweWspFormularz){


        if (suroweWspFormularz.getRokPocz()>suroweWspFormularz.getRokKon() || suroweWspFormularz.getWiekDolny()>suroweWspFormularz.getWiekGorny()){
            boolean wskaznikLat = true;
            boolean wskaznikWieku = true;
            //wywołanie obiektu formularza

            // Wywołanie obiektów rak płuc to listy
            List<RakPluc> raczki = (List<RakPluc>) rakPlucService.findAll();
            raczki.add(new RakPluc(33L, "Nowotwory płuc ogółem"));


            //wywołanie obiektów plec do listy
            List<Plec> plcie =  (List<Plec>) plecService.findAll();
            plcie.add(new Plec(3L, "ogółem"));

            //wywołanie obiektów czynnikRyz do listy
            List<CzynnikRyz> czynnikRyzyka = (List<CzynnikRyz>) czynnikRyzService.findAll();
            czynnikRyzyka.add(new CzynnikRyz(7L, "Ogółem"));



            List<Integer> rokPoczs = czynnikiFormDataProviderService.getRokPocz();
            List<Integer> rokKons = czynnikiFormDataProviderService.getRokKon();
            List<Integer> wiekDolnys = czynnikiFormDataProviderService.getWiekDolny();
            List<Integer> wiekGornys = czynnikiFormDataProviderService.getWiekGorny();
            List<String> zachorowaniaZgony = czynnikiFormDataProviderService.getZachorowaniaZgony();

            model.addAttribute("suroweFormularz", suroweWspFormularz);
            model.addAttribute("plcie", plcie);
            model.addAttribute("raczki", raczki);
            model.addAttribute("czynnikiRyz", czynnikRyzyka);


            model.addAttribute("rokPoczs", rokPoczs);
            model.addAttribute("rokKons", rokKons);
            model.addAttribute("wiekDolnys", wiekDolnys);
            model.addAttribute("wiekGornys", wiekGornys);
            model.addAttribute("zachorowaniaZgonys", zachorowaniaZgony);


            if (suroweWspFormularz.getWiekDolny()>suroweWspFormularz.getWiekGorny()){
                wskaznikWieku = false;
            }
            if(suroweWspFormularz.getRokPocz()>suroweWspFormularz.getRokKon()){
                wskaznikLat = false;

            }

            model.addAttribute("wskaznikLat", wskaznikLat);
            model.addAttribute("wskaznikWieku", wskaznikWieku);



            return "formularz_filtrowania_surowe";
        }
        else {
            Map<String, Float> test = suroweWspFiltrService.wojewodztwaSuroweWsp(suroweWspFormularz);

            if (test.isEmpty()){
                test.put("Brak danych z zadanego zakresu", 0f);
            }
            model.addAttribute("test", test);
            return "liczbaTest2";
        }




    }

    @GetMapping("/formularz_filtrowanie_procenty_nowotwory")
    public String filtrowanieDoProcentowNowotwory(Model model){

        boolean wskaznikWieku = true;
        boolean wskaznikLat = true;
        //wywołanie obiektu formularza
       NowotworyProcentyFormularz nowotworyProcentyFormularz = new NowotworyProcentyFormularz();


        // Wywołanie obiektów rak płuc to listy



        //wywołanie obiektów plec do listy
        List<Plec> plcie =  (List<Plec>) plecService.findAll();
        plcie.add(new Plec(3L, "ogółem"));

        //wywołanie obiektów czynnikRyz do listy
        List<CzynnikRyz> czynnikRyzyka = (List<CzynnikRyz>) czynnikRyzService.findAll();
        czynnikRyzyka.add(new CzynnikRyz(7L, "Ogółem"));



        List<Integer> rokPoczs = czynnikiFormDataProviderService.getRokPocz();
        List<Integer> rokKons = czynnikiFormDataProviderService.getRokKon();
        List<Integer> wiekDolnys = czynnikiFormDataProviderService.getWiekDolny();
        List<Integer> wiekGornys = czynnikiFormDataProviderService.getWiekGorny();
        List<String> zachorowaniaZgony = czynnikiFormDataProviderService.getZachorowaniaZgony();

        model.addAttribute("formularz", nowotworyProcentyFormularz);
        model.addAttribute("plcie", plcie);

        model.addAttribute("czynnikiRyz", czynnikRyzyka);


        model.addAttribute("rokPoczs", rokPoczs);
        model.addAttribute("rokKons", rokKons);
        model.addAttribute("wiekDolnys", wiekDolnys);
        model.addAttribute("wiekGornys", wiekGornys);
        model.addAttribute("zachorowaniaZgonys", zachorowaniaZgony);

        model.addAttribute("wskaznikWieku", wskaznikWieku);
        model.addAttribute("wskaznikLat", wskaznikLat);

        return "formularz_filtrowania_nowotwory_procenty";
    }

    @PostMapping("/formularz_filtrowanie_procenty_nowotwory")
    public String procentyFiltruj(Model model, @ModelAttribute("formularz") NowotworyProcentyFormularz nowotworyProcentyFormularz){

        if (nowotworyProcentyFormularz.getRokKon()<nowotworyProcentyFormularz.getRokPocz() || nowotworyProcentyFormularz.getWiekDolny()>nowotworyProcentyFormularz.getWiekGorny()){
            boolean wskaznikWieku = true;
            boolean wskaznikLat = true;

            // Wywołanie obiektów rak płuc to listy



            //wywołanie obiektów plec do listy
            List<Plec> plcie =  (List<Plec>) plecService.findAll();
            plcie.add(new Plec(3L, "ogółem"));

            //wywołanie obiektów czynnikRyz do listy
            List<CzynnikRyz> czynnikRyzyka = (List<CzynnikRyz>) czynnikRyzService.findAll();
            czynnikRyzyka.add(new CzynnikRyz(7L, "Ogółem"));



            List<Integer> rokPoczs = czynnikiFormDataProviderService.getRokPocz();
            List<Integer> rokKons = czynnikiFormDataProviderService.getRokKon();
            List<Integer> wiekDolnys = czynnikiFormDataProviderService.getWiekDolny();
            List<Integer> wiekGornys = czynnikiFormDataProviderService.getWiekGorny();
            List<String> zachorowaniaZgony = czynnikiFormDataProviderService.getZachorowaniaZgony();

            model.addAttribute("formularz", nowotworyProcentyFormularz);
            model.addAttribute("plcie", plcie);

            model.addAttribute("czynnikiRyz", czynnikRyzyka);


            model.addAttribute("rokPoczs", rokPoczs);
            model.addAttribute("rokKons", rokKons);
            model.addAttribute("wiekDolnys", wiekDolnys);
            model.addAttribute("wiekGornys", wiekGornys);
            model.addAttribute("zachorowaniaZgonys", zachorowaniaZgony);

            if (nowotworyProcentyFormularz.getWiekGorny()<nowotworyProcentyFormularz.getWiekDolny()){
                wskaznikWieku = false;
            }
            if (nowotworyProcentyFormularz.getRokPocz()>nowotworyProcentyFormularz.getRokKon()){
                wskaznikLat = false;
            }

            model.addAttribute("wskaznikWieku", wskaznikWieku);
            model.addAttribute("wskaznikLat", wskaznikLat);

            return "formularz_filtrowania_nowotwory_procenty";
        }
        else{
            Map<String, Float> test = nowotworyProcentyFiltrService.nowotworyUdzialy(nowotworyProcentyFormularz);

            if (test.isEmpty()){
                test.put("Brak danych z zadanego zakresu", 0f);
            }
            model.addAttribute("test", test);
            return "liczbaTest3";
        }



    }


//    @GetMapping("/get-data")
//    public ResponseEntity<Map<String, Integer>> getPieChart() {
//        Map<String, Integer> graphData = new TreeMap<>();
//        graphData.put("2016", 147);
//        graphData.put("2017", 1256);
//        graphData.put("2018", 3856);
//        graphData.put("2019", 19807);
//        return new ResponseEntity<>(graphData, HttpStatus.OK);
//    }


}
