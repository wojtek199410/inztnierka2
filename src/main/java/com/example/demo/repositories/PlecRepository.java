package com.example.demo.repositories;

import com.example.demo.model.entities.Plec;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PlecRepository  extends JpaRepository<Plec,Long> {

    Optional<Plec> findByNazwa(String nazwa);

}
