package com.example.demo.repositories;

import com.example.demo.model.entities.Wojewodztwo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface WojewodztwoRepository extends JpaRepository<Wojewodztwo,Long> {

    Optional<Wojewodztwo> findByNazwa(String nazwa);

}
