package com.example.demo.repositories;

import com.example.demo.model.entities.RakPluc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RakPlucRepository extends JpaRepository<RakPluc,Long> {

    Optional<RakPluc> findByNazwa(String nazwa);

}
