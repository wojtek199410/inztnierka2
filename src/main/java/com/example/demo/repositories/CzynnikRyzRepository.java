package com.example.demo.repositories;

import com.example.demo.model.entities.CzynnikRyz;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CzynnikRyzRepository extends JpaRepository<CzynnikRyz,Long> {

    Optional<CzynnikRyz> findByNazwa(String nazwa);

}
