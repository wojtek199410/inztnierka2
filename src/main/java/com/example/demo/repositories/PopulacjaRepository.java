package com.example.demo.repositories;


import com.example.demo.model.entities.Populacja;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PopulacjaRepository extends JpaRepository<Populacja, Long> {

    Iterable<Populacja> findByWojeAndWiekGreaterThanEqualAndWiekLessThanEqualAndRok(String woje, int wiek1,int wiek2, int rok);
    Iterable<Populacja> findByWiekGreaterThanEqualAndWiekLessThanEqualAndRok(int wiek1, int wiek2, int rok);
    Iterable<Populacja> findByWojeAndWiekGreaterThanEqualAndWiekLessThanEqualAndRokGreaterThanEqualAndRokLessThanEqual(String woje, int wiek1,int wiek2, int rok1, int rok2);
    Iterable<Populacja> findByWiekGreaterThanEqualAndWiekLessThanEqualAndRokGreaterThanEqualAndRokLessThanEqual(int wiek1, int wiek2, int rok1, int rok2);
}
