package com.example.demo.repositories;

import com.example.demo.model.entities.Pacjent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PacjentRepository extends JpaRepository<Pacjent,Long> {

    Optional<Pacjent> findByPesel(String pesel);

//
    Iterable<Pacjent> findByRakPlucId(Long id);
    Iterable<Pacjent> findByCzynnikRyzykaContaining(Long id);
}
